# README #

1/3スケールの日立 ベーシックマスターLEVEL1風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはAUTODESK 123D DESIGNです。

***

# 実機情報

## メーカ
- 日立製作所

## 発売時期
- ベーシックマスター 1978年9月
- ベーシックマスターレベル2 1979年2月
- ベーシックマスターレベル2 II 1980年

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/%E3%83%99%E3%83%BC%E3%82%B7%E3%83%83%E3%82%AF%E3%83%9E%E3%82%B9%E3%82%BF%E3%83%BC)
- [IPSJ コンピュータ博物館](http://museum.ipsj.or.jp/computer/personal/0003.html)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_basicmasterlevel1/raw/941444bacda42c1a0a9dcb822e3b2ff41e2ad038/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_basicmasterlevel1/raw/941444bacda42c1a0a9dcb822e3b2ff41e2ad038/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_basicmasterlevel1/raw/941444bacda42c1a0a9dcb822e3b2ff41e2ad038/ExampleImage.png)
